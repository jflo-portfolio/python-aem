"""Test AEM Module."""
from __future__ import print_function

import urllib.parse

import host_info
import pytest
import vcr
from future import standard_library

import aem
from aem.sling import SlingError

standard_library.install_aliases()

HOST = host_info.HOST
USERNAME = host_info.USERNAME
PASSWORD = host_info.PASSWORD


@vcr.use_cassette("fixtures/aem_test.yml")
@pytest.fixture(scope="class")
def cms():
    """Create an aem.Api object."""
    cms_obj = aem.api(HOST, (USERNAME, PASSWORD))
    return cms_obj


def get_key_list(jobj):
    """Return a list of non 'jcr:' keys."""
    key_list = list()
    for k in list(jobj.keys()):
        if "jcr:" not in k:
            key_list.append(k)
    return key_list


class TestAEM:
    """Class for AEM test."""

    @vcr.use_cassette("fixtures/sling_get.yml")
    def test_get(self, cms):
        """Test SlingApi.get method."""
        jobj = cms.get("/content/python-aem-test-content/en/products/jcr:content.json")
        title = jobj.get("jcr:title")
        print(title)
        assert title == "Products"

    @vcr.use_cassette("fixtures/sling_get_fails.yml")
    def test_get_fails(self, cms):
        """Test SlingApi.get failure."""
        with pytest.raises(SlingError):
            print(cms.get("/content/python-aem-test-content/en/not_exist"))

    @vcr.use_cassette("fixtures/sling_post.yml")
    def test_post(self, cms):
        """Test SlingApi.post method."""
        # create a new attribute
        payload = {"new_attribute": "new_content"}
        cms.post(
            "/content/python-aem-test-content/en/products/jcr:content.json",
            payload=payload,
        )

        # get the attribute and verify that it is the same data we posted
        jobj = cms.get("/content/python-aem-test-content/en/products/jcr:content.json")
        new_attribute = jobj.get("new_attribute")
        print(new_attribute)
        assert new_attribute == "new_content"

    @vcr.use_cassette("fixtures/sling_copy.yml")
    def test_copy(self, cms):
        """Test SlingApi.copy method."""
        src = "/content/python-aem-test-content/en/products"
        dest = "/content/python-aem-test-content/en/products2"
        cms.copy(src, dest)
        jobj = cms.get("/content/python-aem-test-content/en/products2/jcr:content.json")
        title = jobj.get("jcr:title")
        print(title)
        assert title == "Products"
        cms.delete(dest)

    @vcr.use_cassette("fixtures/sling_copy_fails.yml")
    def test_copy_fails(self, cms):
        """Test SlingApi.copy failure."""
        src = "/content/python-aem-test-content/en/products"
        dest = "/content/python-aem-test-content/en/products"
        with pytest.raises(SlingError):
            print(cms.copy(src, dest))

    @vcr.use_cassette("fixtures/sling_move.yml")
    def test_move(self, cms):
        """Test SlingApi.move method."""
        # first create a products2 node for testing
        cms.copy(
            "/content/python-aem-test-content/en/products",
            "/content/python-aem-test-content/en/products2",
        )

        src = "/content/python-aem-test-content/en/products2"
        dest = "/content/python-aem-test-content/en/products3"
        cms.move(src, dest)

        # verify that the moved node contains same data
        jobj = cms.get("/content/python-aem-test-content/en/products3/jcr:content.json")
        title = jobj.get("jcr:title")
        print(title)
        assert title == "Products"

    @vcr.use_cassette("fixtures/sling_move_fails.yml")
    def test_move_fails(self, cms):
        """Test SlingApi.move failure."""
        # try copying a a node onto itself, this should raise an error
        src = "/content/python-aem-test-content/en/products"
        with pytest.raises(SlingError):
            print(cms.move(src, src))

    @vcr.use_cassette("fixtures/sling_delete.yml")
    def test_delete(self, cms):
        """Test SlingApi.delete method."""
        cms.delete("/content/python-aem-test-content/en/products3")
        # check if deleted node exists
        with pytest.raises(SlingError):
            print(cms.get("/content/python-aem-test-content/en/products3"))

    @vcr.use_cassette("fixtures/sling_delete_fails.yml")
    def test_delete_fails(self, cms):
        """Test SlingApi.delete failure."""
        p_node = "/content/python-aem-test-content/en/products/triangle/rep:policy"
        with pytest.raises(SlingError):
            # policy nodes can't be deleted, so this should raise an exception
            cms.delete(p_node)

    @vcr.use_cassette("fixtures/sling_safe_move.yml")
    def test_safe_move(self, cms):
        """Test SlingApi.safe_move method."""
        cms.copy(
            "/content/python-aem-test-content/en/products",
            "/content/python-aem-test-content/en/products4",
        )

        src = "/content/python-aem-test-content/en/products4"
        dest = "/content/python-aem-test-content/en/products5"
        cms.safe_move(src, dest)
        jobj = cms.get("/content/python-aem-test-content/en/products5/jcr:content.json")
        title = jobj.get("jcr:title")
        print(title)
        assert title == "Products"
        cms.delete("/content/python-aem-test-content/en/products5")

    @vcr.use_cassette("fixtures/sling_safe_move_fails.yml")
    def test_safe_move_fails(self, cms):
        """Test SlingApi.safe_move failure."""
        src = "/content/python-aem-test-content/en/products"
        with pytest.raises(SlingError):
            # copying node to itself should raise an exception
            cms.safe_move(src, src)

    @vcr.use_cassette("fixtures/sling_query.yml")
    def test_query(self, cms):
        """Test Api.query method."""
        # query params
        q_params = {
            "path": "/content/python-aem-test-content",
            "type": "cq:PageContent",
            "p.hits": "full",
            "p.limit": "3",
            "p.nodedepth": "2",
        }
        results = cms.query(q_params)
        success = results.get("success")
        num_results = results.get("results")
        hits = results.get("hits")
        hit_count = len(hits)

        assert success is True
        assert num_results == 3
        assert hit_count == 3

    @vcr.use_cassette("fixtures/sling_query_url.yml")
    def test_query_url(self, cms):
        """Test Api.query_url method."""
        # query params
        q_params = {
            "path": "/content/python-aem-test-content",
            "type": "cq:PageContent",
            "p.hits": "full",
            "p.limit": "3",
            "p.nodedepth": "2",
        }
        url = cms.query_url(q_params)
        parsed = urllib.parse.urlparse(url)

        assert HOST == "{0}://{1}".format(parsed.scheme, parsed.netloc)
        print(url)
        print(parsed.query)
        p_list = [
            "path=%2Fcontent%2Fpython-aem-test-content",
            "type=cq%3APageContent",
            "p.hits=full",
            "p.limit=3",
            "p.nodedepth=2",
        ]

        for i in p_list:
            assert i in parsed.query

    @vcr.use_cassette("fixtures/sling_upload.yml")
    def test_upload(self, cms):
        """Test Api.upload method."""
        cms.upload("tests/data/test_upload_package.zip", install=True)
        jobj = cms.get(
            "/content/python-aem-test-content/en/uploaded_content/jcr:content.json"
        )
        title = jobj.get("jcr:title")
        print(title)
        assert title == "Products"
        cms.delete("/content/python-aem-test-content/en/uploaded_content")

    @vcr.use_cassette("fixtures/sling_order_first.yml")
    def test_order_first(self, cms):
        """
        Test SlingApi.order method.

        using 'first' for the placement parameter
        """
        path = "/content/python-aem-test-content/en/products"

        # test 'first'
        cms.order(path, "first")
        jobj = cms.get("content/python-aem-test-content/en.1.json")
        key_list = get_key_list(jobj)
        assert key_list[0] == "products"

    @vcr.use_cassette("fixtures/sling_order_last.yml")
    def test_order_last(self, cms):
        """
        Test SlingApi.order method.

        using 'last' for the placement parameter
        """
        path = "/content/python-aem-test-content/en/products"

        # test 'last'
        cms.order(path, "last")
        jobj = cms.get("content/python-aem-test-content/en.1.json")
        key_list = get_key_list(jobj)
        assert key_list[-1] == "products"

    @vcr.use_cassette("fixtures/sling_order_before.yml")
    def test_order_before(self, cms):
        """
        Test SlingApi.order method.

        using 'before' for the placement parameter
        """
        path = "/content/python-aem-test-content/en/products"

        # test 'before'
        cms.order(path, "before", "first")
        jobj = cms.get("content/python-aem-test-content/en.1.json")
        key_list = get_key_list(jobj)
        first_index = key_list.index("first")
        products_index = key_list.index("products")
        assert products_index == first_index - 1

    @vcr.use_cassette("fixtures/sling_order_before_failure.yml")
    def test_order_before_failure(self, cms):
        """Test SlingApi.order method failure.

        using 'before' for the placement parameter
        """
        path = "/content/python-aem-test-content/en/products"

        # test 'before' failure
        with pytest.raises(ValueError):
            # a 'sibling' node needs to be specified
            cms.order(path, "before")

    @vcr.use_cassette("fixtures/sling_order_after.yml")
    def test_order_after(self, cms):
        """Test SlingApi.order method.

        using 'after' for the placement parameter
        """
        path = "/content/python-aem-test-content/en/products"

        # test 'after'
        cms.order(path, "after", "first")
        jobj = cms.get("content/python-aem-test-content/en.1.json")
        key_list = get_key_list(jobj)
        first_index = key_list.index("first")
        products_index = key_list.index("products")
        assert products_index == first_index + 1

    @vcr.use_cassette("fixtures/sling_order_after_failure.yml")
    def test_order_after_failure(self, cms):
        """Test SlingApi.order method failure.

        using 'after' for the placement parameter
        """
        path = "/content/python-aem-test-content/en/products"

        # test 'after' failure
        with pytest.raises(ValueError):
            # a 'sibling' node needs to be specified
            cms.order(path, "after")

    @vcr.use_cassette("fixtures/sling_order_number.yml")
    def test_order_number(self, cms):
        """Test  SlingApi.order method.

        using a number for the placement parameter
        """
        path = "/content/python-aem-test-content/en/products"

        # test order by number
        cms.order(path, "3")
        jobj = cms.get("content/python-aem-test-content/en.1.json")
        key_list = get_key_list(jobj)
        assert key_list[2] == "products"

    @vcr.use_cassette("fixtures/sling_order_number_failure.yml")
    def test_order_number_failure(self, cms):
        """
        Test SlingApi.order method failure.

        using a number for the placement parameter
        """
        path = "/content/python-aem-test-content/en/products"
        # test failure
        with pytest.raises(ValueError):
            # requires a valid 'placement' value
            cms.order(path, "gibberish")

    @vcr.use_cassette("fixtures/sling_import.yml")
    def test_import(self, cms):
        """Test Sling.import_content method."""
        path = "/content/python-aem-test-content/en/products"

        # import without name parameter
        jcontent = """{ "sample3":
        { "jcr:primaryType": "cq:Page",
          "jcr:content" : { "jcr:primaryType" : "cq:PageContent",
          "jcr:title": "sample3" } }}"""
        cms.import_content(path, jcontent)
        sample3_data = (
            "/content/python-aem-test-content/en/products/sample3/jcr:content.json"
        )
        jobj = cms.get(sample3_data)
        assert jobj.get("jcr:title", "") == "sample3"

        # test failure
        jcontent2 = """{ "jcr:primaryType": "cq:Page",
        "jcr:content" : { "jcr:primaryType" : "cq:PageContent",
        "jcr:title": "sample4" } }"""
        with pytest.raises(SlingError):
            # should throw error when node already exists
            cms.import_content(path, jcontent2, "sample3")

        # delete samples after tests
        cms.delete("/content/python-aem-test-content/en/products/sample3")

    @vcr.use_cassette("fixtures/sling_import_name.yml")
    def test_import_name(self, cms):
        """Test Sling.import_content method with name."""
        path = "/content/python-aem-test-content/en/products"

        # import with name parameter
        jcontent2 = """{ "jcr:primaryType": "cq:Page",
        "jcr:content" : { "jcr:primaryType" : "cq:PageContent",
        "jcr:title": "sample4" } }"""
        cms.import_content(path, jcontent2, "sample4")
        sample4_data = (
            "/content/python-aem-test-content/en/products/sample4/jcr:content.json"
        )
        jobj = cms.get(sample4_data)
        assert jobj.get("jcr:title", "") == "sample4"

        # delete samples after tests
        cms.delete("/content/python-aem-test-content/en/products/sample4")

    @vcr.use_cassette("fixtures/sling_import_replace.yml")
    def test_import_replace(self, cms):
        """Test Sling.import_content method with replace."""
        path = "/content/python-aem-test-content/en/products"

        # import replace
        jcontent3 = """{ "sample3":
        { "jcr:primaryType": "cq:Page",
          "jcr:content" : { "jcr:primaryType" : "cq:PageContent",
          "jcr:title": "sample3-replace" } }}"""
        cms.import_content(path, jcontent3, replace=True)
        sample3_data = (
            "/content/python-aem-test-content/en/products/sample3/jcr:content.json"
        )
        jobj = cms.get(sample3_data)
        assert jobj.get("jcr:title", "sample3-replace") == "sample3-replace"

        # delete samples after tests
        cms.delete("/content/python-aem-test-content/en/products/sample3")

    @vcr.use_cassette("fixtures/sling_import_replace_properties.yml")
    def test_import_replace_properties(self, cms):
        """Test Sling.import_content method with replace."""
        path = "/content/python-aem-test-content/en/products"

        # import replace_properties
        jcontent4 = """{ "sample3":
        { "jcr:primaryType": "cq:Page",
          "jcr:content" : { "jcr:primaryType" : "cq:PageContent",
          "jcr:title": "sample3-replace_props" } }}"""
        cms.import_content(path, jcontent4, replace_properties=True)
        sample3_data = (
            "/content/python-aem-test-content/en/products/sample3/jcr:content.json"
        )
        jobj = cms.get(sample3_data)
        assert jobj.get("jcr:title") == "sample3-replace_props"

        # delete samples after tests
        cms.delete("/content/python-aem-test-content/en/products/sample3")

    @vcr.use_cassette("fixtures/sling_import_file.yml")
    def test_import_file(self, cms):
        """Test Sling.import_file method."""
        path = "/content/python-aem-test-content/en/products"
        # cms.import_file(path, 'tests/data/test_import.json')

        # import a file
        cms.import_file(path, "tests/data/triangle.infinity.json", name="sample6")

        jpath = "/content/python-aem-test-content/en/products/sample6/jcr:content.json"
        jobj = cms.get(jpath)
        assert jobj.get("jcr:title") == "Triangle"

        # test failure
        with pytest.raises(SlingError):
            # should throw error when node already exists
            cms.import_file(path, "tests/data/triangle.infinity.json", name="sample6")

        # test import replace
        cms.import_file(
            path,
            "tests/data/triangle.infinity.json",
            name="sample6",
            replace=True,
        )
        jobj = cms.get(jpath)
        assert jobj.get("jcr:title") == "Triangle"

        # delete samples after tests
        cms.delete("/content/python-aem-test-content/en/products/sample5")
        cms.delete("/content/python-aem-test-content/en/products/sample6")


"""
    These are commented out b/c testing these without a publisher may block
    the replication queue.

    def test_activate(self, cms):
        cms.activate('/content/python-aem-test-content/en/products5/triangle/overview')

    def test_deactivate(self, cms):
        cms.deactivate('/content/python-aem-test-content/en/products5/triangle/overview')

    def test_tree_activate(self, cms):
        cms.tree_activate('/content/python-aem-test-content/en/products5',
                          onlymodified=False)
"""

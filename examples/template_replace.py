#!/usr/bin/env python
"""Script that replaces all pages of a given template type to another template."""

import click

import aem


@click.command()
@click.argument("host")
@click.argument("path")
@click.argument("tmpl")
@click.argument("tmpl_replace")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
@click.option("-d", "--dryrun", is_flag=True, help="dry run")
def main(host, username, password, path, tmpl, tmpl_replace, dryrun):
    """Replace all pages of a given template type to another template."""
    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # query params
    q_params = {
        "path": path,
        "type": "cq:PageContent",
        "property": "cq:template",
        "property.value": tmpl,
        "orderby": "path",
        "orderby.sort": "asc",
        "p.limit": "-1",
    }
    results = cms.query(q_params)

    for hits in results.get("hits"):
        click.echo(hits.get("path"))
        if not dryrun:
            cms.post(hits.get("path"), {"cq:template": tmpl_replace})


if __name__ == "__main__":
    main()

#!/usr/bin/env python
"""Script to find components in content."""

import click

import aem


@click.command()
@click.argument("host")
@click.argument("path")
@click.argument("component")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
@click.option("-q", "--query", is_flag=True, help="print query")
def main(host, username, password, path, query, component):
    """Find components in content."""
    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # query params
    q_params = {
        "path": path,
        "property": "sling:resourceType",
        "property.value": component,
        "orderby": "jcr:path",
        "orderby.sort": "asc",
        "p.hits": "selective",
        "p.properties": "sling:resourceType jcr:path",
        "p.limit": "-1",
    }

    if query:
        # print the full query url
        print(cms.query_url(q_params))
    else:
        results = cms.query(q_params)

        for hits in results.get("hits"):
            print(hits.get("jcr:path"))


if __name__ == "__main__":
    main()

#!/usr/bin/env python
"""Querybuilder Script."""

from __future__ import print_function

import json
import urllib.error
import urllib.parse
import urllib.request

import click
from future import standard_library

import aem

standard_library.install_aliases()


@click.command()
@click.argument("host")
@click.argument("query")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
def main(host, username, password, query):
    """Execute a query."""
    query = urllib.parse.unquote(query)
    param_list = query.lstrip("?").split("&")

    q_params = dict()
    for i in param_list:
        key, value = i.split("=")
        q_params[key] = value

    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    print(json.dumps(cms.query(q_params)))


if __name__ == "__main__":
    main()

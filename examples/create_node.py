#!/usr/bin/env python
"""Script that creates a node."""

import click

import aem


@click.command()
@click.argument("host")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
def main(host, username, password):
    """Script that creates a node."""
    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # create a node at:
    # /content/geometrixx/en/company/management/jcr:content/sample3
    path = "/content/geometrixx/en/company/management/jcr:content"
    content = """{ "jcr:primaryType" : "nt:unstructured"}"""
    cms.import_content(path, content, "sample3")


if __name__ == "__main__":
    main()

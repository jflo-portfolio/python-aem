#!/usr/bin/env python
import click

import aem


@click.command()
@click.argument("host")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
def main(host, username, password):
    """
    Script that prints out all the users in the CMS
    """

    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # query params
    q_params = {
        "path": "/home/users",
        "type": "rep:User",
        "orderby": "path",
        "orderby.sort": "asc",
        "p.hits": "full",
        "p.limit": "-1",
    }
    results = cms.query(q_params)

    for h in results.get("hits"):
        full_name = h.get("rep:fullname")

        if not full_name:
            full_name = h.get("rep:principalName")

        click.echo(full_name)


if __name__ == "__main__":
    main()

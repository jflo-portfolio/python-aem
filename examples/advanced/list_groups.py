#!/usr/bin/env python
import click
import tablib

import aem


@click.command()
@click.argument("host")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
@click.option("-t", "--file_type", default="csv", help="type: csv or xls")
def main(host, username, password, file_type):
    """
    Lists all the groups in the CMS
    """

    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # query params
    q_params = {
        "path": "/home/groups",
        "type": "rep:Group",
        "orderby": "path",
        "orderby.sort": "asc",
        "p.hits": "full",
        "p.limit": "-1",
        "p.nodedepth": "2",
    }
    results = cms.query(q_params)

    # create table
    data = tablib.Dataset()
    data.headers = ["ID", "Name"]

    for h in results.get("hits"):
        principal_name = h.get("rep:principalName", "")

        profile = h.get("profile", None)
        if profile:
            given_name = profile.get("givenName", "")
            if given_name:
                data.append([principal_name, given_name])
        else:
            data.append([principal_name, principal_name])

    # output
    stdout_text = click.get_text_stream("stdout")
    if file_type.lower() == "xls":
        stdout_text.write(data.xls)
    else:
        stdout_text.write(data.csv)
    stdout_text.close()


if __name__ == "__main__":
    main()

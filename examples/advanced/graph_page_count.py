#!/usr/bin/env python
import os.path
import webbrowser

import click
import pygal
from pygal.style import DarkStyle

import aem


@click.command()
@click.argument("host")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
def main(host, username, password):
    """
    Script that graphs the page counts of the various geometrixx sections
    """
    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # query params
    q_params = {
        "type": "cq:PageContent",
        "orderby": "path",
        "orderby.sort": "asc",
        "p.limit": "1",
        "p.hits": "full",
    }

    sites = [
        "geometrixx",
        "geometrixx-outdoors-mobile",
        "geometrixx-media",
        "geometrixx-outdoors",
        "geometrixx_mobile",
    ]
    page_totals = list()

    for s in sites:
        q_params["path"] = "/content/{0}".format(s)
        results = cms.query(q_params)

        page_totals.append(results.get("total"))

    # create graph
    line_chart = pygal.HorizontalBar(style=DarkStyle)
    line_chart.title = "Page Count"

    line_chart.x_labels = sites

    line_chart.add("Number of Pages", page_totals)

    line_chart.render_to_file("graph_page_count.svg")
    webbrowser.open("file://" + os.path.realpath("graph_page_count.html"))


if __name__ == "__main__":
    main()

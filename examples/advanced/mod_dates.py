#!/usr/bin/env python
import click
import tablib
from dateutil.parser import parse

import aem


@click.command()
@click.argument("host")
@click.argument("path")
@click.argument("node_type")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
@click.option("-t", "--file_type", default="csv", help="type: csv or xls")
def main(host, username, password, path, node_type, file_type):
    """
    Script that creates a spreadsheet showing the modification dates of nodes
    in a specified path
    """

    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # query params
    q_params = {
        "path": path,
        "type": node_type,
        "orderby": "@jcr:lastModified",
        "orderby.sort": "desc",
        "p.limit": "-1",
        "p.hits": "full",
    }
    results = cms.query(q_params)

    # create table
    data = tablib.Dataset()
    data.headers = ["Path", "Last Modified", "Last Modified By"]
    for h in results.get("hits"):
        path = h.get("jcr:path", "")

        mod_date = h.get("jcr:lastModified", "")
        if not mod_date:
            mod_date = h.get("cq:lastModified", "")

        mod_by = h.get("jcr:lastModifiedBy", "")
        if not mod_by:
            mod_by = h.get("cq:lastModifiedBy", "")

        if mod_date:
            d = parse(mod_date)

            mod_date = d.strftime("%Y-%m-%d %I:%M:%S")

        data.append([path, mod_date, mod_by])

    # output
    stdout_text = click.get_text_stream("stdout")
    if file_type.lower() == "xls":
        stdout_text.write(data.xls)
    else:
        stdout_text.write(data.csv)
    stdout_text.close()


if __name__ == "__main__":
    main()

#!/usr/bin/env python
import click

import aem


@click.command()
@click.argument("host")
@click.argument("path")
@click.argument("component")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
@click.option("-q", "--query", is_flag=True, help="print query")
@click.option("-d", "--dryrun", is_flag=True, help="dry run")
def main(host, username, password, path, query, component, dryrun):
    """
    Script to find components in content
    """
    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # query params
    q_params = {
        "path": path,
        "property": "sling:resourceType",
        "property.value": component,
        "orderby": "jcr:path",
        "orderby.sort": "asc",
        "p.hits": "selective",
        "p.properties": "sling:resourceType jcr:path",
        "p.limit": "-1",
    }

    if query:
        # print the full query url
        print(cms.query_url(q_params))
    else:
        results = cms.query(q_params)

        for h in results.get("hits"):
            print(h.get("jcr:path"))


if __name__ == "__main__":
    main()

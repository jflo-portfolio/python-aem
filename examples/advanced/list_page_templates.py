#!/usr/bin/env python
import click
import tablib

import aem


@click.command()
@click.argument("host")
@click.argument("path")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
@click.option("-t", "--file_type", default="csv", help="type: csv or xls")
def main(host, username, password, path, file_type):
    """
    Script that creates a spreadsheet listing the page and it's template
    """

    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # query params
    q_params = {
        "path": path,
        "type": "cq:PageContent",
        "p.hits": "full",
        "p.limit": "-1",
    }
    results = cms.query(q_params)

    author_host = ""

    # generate spreadsheet
    data = tablib.Dataset()
    data.headers = ["URL", "Template"]
    for h in results.get("hits"):
        page_path = h.get("jcr:path").split("/jcr:content")[0]
        page_url = "{0}{1}.html".format(author_host, page_path)
        data.append([page_url, h.get("cq:template", "")])

    if file_type == "xls":
        print(data.xls)
    else:
        print(data.csv)


if __name__ == "__main__":
    main()

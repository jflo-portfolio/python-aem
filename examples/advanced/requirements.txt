# Install with: `pip install -r requirements.txt`

click >= 4.0
tablib >= 0.10.0
pygal >= 1.7.0
python-dateutil >= 2.6.1

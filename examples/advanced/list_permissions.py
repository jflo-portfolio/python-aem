#!/usr/bin/env python
import itertools

import click
import tablib

import aem

"""
http://localhost:4502/bin/querybuilder.json?path=/&type=rep:ACL&p.hits=full&p.nodedepth=1
"""


class ACE(object):
    def __init__(self, ace_type=None, principal_name=None, privileges=[]):
        self.ace_type = ace_type
        self.principal_name = principal_name
        self.privileges = privileges

    @property
    def access(self):
        if self.ace_type == "rep:GrantACE":
            return "allow"
        elif self.ace_type == "rep:DenyACE":
            return "deny"

    @property
    def permissions(self):
        permission_list = []
        if "jcr:read" in self.privileges:
            permission_list.append("read")
        if (
            "jcr:versionManagement" in self.privileges
            and "jcr:lockManagement" in self.privileges
            and "jcr:modifyProperties" in self.privileges
        ):
            permission_list.append("modify")
        if (
            "jcr:addChildNodes" in self.privileges
            and "jcr:removeChildNodes" in self.privileges
            and "jcr:nodeTypeManagement" in self.privileges
            and "jcr:removeNode" in self.privileges
        ):
            permission_list.append("modify")
        if (
            "jcr:addChildNodes" in self.privileges
            and "jcr:nodeTypeManagement" in self.privileges
        ):
            permission_list.append("create")
        if (
            "jcr:removeChildNodes" in self.privileges
            and "jcr:removeNode" in self.privileges
        ):
            permission_list.append("delete")
        if (
            "jcr:write" in self.privileges
            and "jcr:nodeTypeManagement" in self.privileges
            and "jcr:lockManagement" in self.privileges
        ):
            permission_list.append("create")
            permission_list.append("delete")
        if "rep:write" in self.privileges:
            permission_list.append("modify")
            permission_list.append("create")
            permission_list.append("delete")
        if "jcr:readAccessControl" in self.privileges:
            permission_list.append("read acl")
        if "jcr:readModifyControl" in self.privileges:
            permission_list.append("edit acl")
        if "crx:replicate" in self.privileges:
            permission_list.append("replicate")
        if "jcr:all" in self.privileges:
            permission_list.append("read")
            permission_list.append("modify")
            permission_list.append("create")
            permission_list.append("delete")
            permission_list.append("read acl")
            permission_list.append("edit acl")
            permission_list.append("replicate")

        return ", ".join(set(permission_list))

    @staticmethod
    def from_dict(hit):
        ace_type = hit.get("jcr:primaryType")
        principal_name = hit.get("rep:principalName")
        privileges = hit.get("rep:privileges")
        return ACE(ace_type, principal_name, privileges)


class ACL(object):
    def __init__(self, path=None, ace_list=[]):
        self.path = path
        self.ace_list = ace_list

    @property
    def grouped_ace_list(self):
        """
        grouped by principal_name
        returns only the first ace of each group
        """
        group_list = [
            list(g)[0]
            for k, g in itertools.groupby(self.ace_list, key=lambda x: x.principal_name)
        ]
        return group_list

    @property
    def base_path(self):
        return self.path.split("/rep:policy")[0]

    @staticmethod
    def from_dict(hit):
        path = hit.get("jcr:path")
        ace_list = []
        for k in hit.keys():
            if "allow" in k or "deny" in k:
                # k is an ACE.
                # create an ACE object and add it to the ace_list
                ace_list.append(ACE.from_dict(hit.get(k)))
        return ACL(path, ace_list)


@click.command()
@click.argument("host")
@click.argument("path")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
@click.option("-t", "--file_type", default="csv", help="type: csv or xls")
def main(host, username, password, path, file_type):
    """
    Script that creates a spreadsheet of the permissions inside the CMS
    """

    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # query params
    q_params = {
        "path": path,
        "type": "rep:ACL",
        "p.hits": "full",
        "p.limit": "-1",
        "p.nodedepth": "1",
        "orderby": "path",
        "orderby.sort": "desc",
    }
    results = cms.query(q_params)

    # create table
    data = tablib.Dataset()
    data.headers = ["Path", "Group", "Access", "Privileges"]
    for hit in results.get("hits"):
        c = ACL.from_dict(hit)
        for ace in c.grouped_ace_list:
            if ace.permissions:
                data.append(
                    [c.base_path, ace.principal_name, ace.access, ace.permissions]
                )

    # output
    stdout_text = click.get_text_stream("stdout")
    if file_type.lower() == "xls":
        stdout_text.write(data.xls)
    else:
        stdout_text.write(data.csv)
    stdout_text.close()


if __name__ == "__main__":
    main()

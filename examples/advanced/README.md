# advanced examples

## Install third party libraries:

Make sure you are in the "examples" directory.

    cd python-aem/examples

Install requirements.

    pip install -r requirements.txt


## Running the scripts
All the example scripts have a "--help" option to display parameters and usage.


Script that prints out the pages of a specified path:

    python find_pages.py "http://localhost:4502" "/content/nyu" -u admin

Script that prints out the pages created with a specified template:

    python find_pages_w_template.py "http://localhost:4502" "/content/geometrixx" "/apps/geometrixx/templates/contentpage" -l -1 -u admin

Lists all the groups in the CMS:

    python list_groups.py "http://localhost:4502" -u admin

Script that lists all the groups and their users. This may take a couple of minutes to run:

    python list_groups_users.py "http://localhost:4502" -u admin -p "admin" -t xls > groups_users.xls

Script that lists all the groups and their users. This may take a couple of minutes to run:

    python list_page_templates.py "http://localhost:4502" "/content/geometrixx" -u admin -p "admin" -t xls > path_template.xls

Script that creates a spreadsheet listing the page and it's template:

    python page_count.py "http://localhost:4502" "/content/geometrixx" -u admin -p "admin"

Script that creates a spreadsheet of the permissions inside the CMS:

    python list_permissions.py "http://localhost:4502" "/" -u admin -p "admin" > p.csv

Script to replace the component names in content:

    python component_replace.py "http://localhost:4502" "/content/geometrixx/en/company/management" "geometrixx/components/title" "new_component" -u admin -d

Script to replace the template names in content:

    python template_replace.py "http://localhost:4502" "/content/geometrixx/en/company/management" "/apps/geometrixx/templates/contentpage" "new_template" -u admin -d

Script to list all users:

    python list_users.py "http://localhost:4502" -u admin

Script that creates a spreadsheet showing the modification dates of nodes in a specified path:

    python mod_dates.py "http://localhost:4502" "/content/dam/geometrixx" "dam:AssetContent" -u admin

Script that uses the QueryBuilder:

    python querybuilder.py "http://localhost:4502" "?type=cq:PageContent&p.hits=full" -u admin

Script to find components in content:

    python find_components.py "http://localhost:4502" "foundation/components/text" -u admin

Script to create a node:

    python create_node.py "http://localhost:4502" -u admin

Script that graphs the page counts of the various sites:

    python graph_page_count.py "http://localhost:4502" -u admin

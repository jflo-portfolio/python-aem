#!/usr/bin/env python
"""Script that prints out the pages created with a specified template."""

import click

import aem


@click.command()
@click.argument("host")
@click.argument("path")
@click.argument("template")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
@click.option("-l", "--limit", default="5", help="limit")
def main(host, username, password, path, limit, template):
    """Print out the pages created with a specified template."""
    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # query params
    q_params = {
        "path": path,
        "type": "cq:PageContent",
        "property": "cq:template",
        "property.value": template,
        "orderby": "path",
        "orderby.sort": "asc",
        "p.limit": limit,
    }
    results = cms.query(q_params)

    for hits in results.get("hits"):
        click.echo(hits.get("path"))


if __name__ == "__main__":
    main()

#!/usr/bin/env python
import aem

username = "admin"
password = "admin"

# create an aem api instance
cms = aem.api("http://localhost:4502", (username, password))

cms.upload("python-aem-test-package.zip")

"""
curl equivalent
curl -u admin:admin -F package=@"python-aem-test-package.zip" http://localhost:4502/crx/packmgr/service/.json?cmd=upload
"""

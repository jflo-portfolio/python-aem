#!/usr/bin/env python
import aem

username = "admin"
password = "admin"

# create an aem api instance
cms = aem.api("http://localhost:4502", (username, password))

path = "/content/python-aem-test-content/en/products3"

cms.delete(path)

"""
curl equivalent
curl -u admin:admin -F":operation=delete" http://localhost:4502/content/python-aem-test-content/en/products3
"""

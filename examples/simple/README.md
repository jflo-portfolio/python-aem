# simple examples

These simple examples illustrate some of the basic api usage.

Script that gets data from a node:

    python get_example.py

Script that posts data to a node:

    python post_example.py

Script that copies a node from one place to another:

    python copy_example.py

Script that deletes a node:

    python delete_example.py

Script that moves data from one place to another:

    python safe_move_example.py

Script that orders a node:

    python order_example.py

Script that queries the datastore:

    python query_example.py

Script that uploads a package:

    python upload_example.py

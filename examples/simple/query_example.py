#!/usr/bin/env python
import aem

username = "admin"
password = "admin"

# create an aem api instance
cms = aem.api("http://localhost:4502", (username, password))

# query params
q_params = {
    "path": "/content/python-aem-test-content",
    "type": "cq:PageContent",
    "p.hits": "full",
    "p.limit": "3",
}
results = cms.query(q_params)

print("Results:")
print(results)

print("\nNumber of results:")
print(results.get("results"))

"""
curl equivalent
Use the Query Debugger: http://localhost:4502/libs/cq/search/content/querydebug.html
curl -u admin:admin "http://localhost:4502/bin/querybuilder.json?p.hits=full&p.limit=3&path=/content/python-aem-test-content&type=cq:PageContent"
"""

#!/usr/bin/env python
import aem

username = "admin"
password = "admin"

# create an aem api instance
cms = aem.api("http://localhost:4502", (username, password))

src = "/content/python-aem-test-content/en/products"
dest = "/content/python-aem-test-content/en/products2"

cms.copy(src, dest)

"""
curl equivalent
curl -u admin:admin -F":operation=copy" -F":dest=/content/python-aem-test-content/en/products2" http://localhost:4502/content/python-aem-test-content/en/products
"""

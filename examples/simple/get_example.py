#!/usr/bin/env python
import aem

username = "admin"
password = "admin"

# create an aem api instance
cms = aem.api("http://localhost:4502", (username, password))

path = "/content/python-aem-test-content/en/products/jcr:content.json"
data = cms.get(path)  # gets the data and converts it to an OrderedDict

print("Data:")
print(data)

# get 'cq:lastModified' key from the OrderedDict
last_modified_by = data.get("cq:lastModifiedBy")
print("\nLast Modified By:")
print(last_modified_by)

"""
curl equivalent
curl -u admin:admin http://localhost:4502/content/python-aem-test-content/en/products/jcr:content.json
"""

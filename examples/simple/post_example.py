#!/usr/bin/env python
import aem

username = "admin"
password = "admin"

# create an aem api instance
cms = aem.api("http://localhost:4502", (username, password))

post_data = {"test_property": "test content"}

path = "/content/python-aem-test-content/en/products/jcr:content.json"
cms.post(path, payload=post_data)

"""
curl equivalent
curl -u admin:admin -Ftest_property="test content" http://localhost:4502/content/python-aem-test-content/en/products/jcr:content
"""

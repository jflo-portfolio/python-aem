#!/usr/bin/env python
import aem

username = "admin"
password = "admin"

# create an aem api instance
cms = aem.api("http://localhost:4502", (username, password))

path = "/content/python-aem-test-content/en/products"

# first
cms.order(path, "first")

# last
# cms.order(path, 'last')

# before
# cms.order(path, 'before', '/content/python-aem-test-content/en/products/first')

# after
# cms.order(path, 'after', '/content/python-aem-test-content/en/products/second')

# order number
# cms.order(path, '3')


"""
curl equivalents

# first
curl -u admin:admin -F":order=first" http://localhost:4502/content/python-aem-test-content/en/products

# last
curl -u admin:admin -F":order=last" http://localhost:4502/content/python-aem-test-content/en/products

# before
curl -u admin:admin -F":order=before first" http://localhost:4502/content/python-aem-test-content/en/products

# after
curl -u admin:admin -F":order=after second" http://localhost:4502/content/python-aem-test-content/en/products

# order number
curl -u admin:admin -F":order=3" http://localhost:4502/content/python-aem-test-content/en/products
"""

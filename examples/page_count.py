#!/usr/bin/env python
"""Script that prints out the number of pages in a specified path."""

import click

import aem


@click.command()
@click.argument("host")
@click.argument("path")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
@click.option(
    "-r",
    "--rep_type",
    default=None,
    help="replication type. either 'activate' or 'deactivate'",
)
def main(host, username, password, path, rep_type):
    """Print out the number of pages in a specified path."""
    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # query params
    q_params = {
        "path": path,
        "type": "cq:PageContent",
        "p.hits": "full",
        "p.nodedepth": "2",
    }

    if rep_type:
        if rep_type.lower().startswith("d"):
            replication_type = "Deactivate"
        else:
            replication_type = "Activate"

        q_params["1_property"] = "cq:lastReplicationAction"
        q_params["1_property.value"] = replication_type

    results = cms.query(q_params)

    num_pages = results.get("total")

    rep_string = ""
    if rep_type:
        rep_string = replication_type.lower() + "d"

    print("There are {0} {1} pages in {2}.".format(num_pages, rep_string, path))


if __name__ == "__main__":
    main()

#!/usr/bin/env python
"""Script that lists all the groups and their users."""

import re

import click
import tablib

import aem


@click.command()
@click.argument("host")
@click.option("-u", "--username", default="admin", help="username")
@click.option("-p", "--password", prompt=True, hide_input=True, help="password")
@click.option("-t", "--file_type", default="csv", help="type: csv or xls")
def main(host, username, password, file_type):
    """List all the groups and their users."""
    # create api instance
    auth = (username, password)
    cms = aem.api(host, auth)

    # query params
    q_params = {
        "path": "/home/groups",
        "type": "rep:Group",
        "p.hits": "full",
        "p.limit": "-1",
    }
    # all the groups
    g_results = cms.query(q_params)

    # create the table
    data = tablib.Dataset()
    data.headers = ["Group", "NetID", "Full Name", "Email"]

    # loop through each group and get the users for that group
    for grp in g_results.get("hits"):
        try:
            # groups with members
            if grp.get("rep:members"):
                for member_id in grp.get("rep:members"):
                    # query for user
                    q_params = {
                        "property": "jcr:uuid",
                        "property.value": member_id,
                        "p.hits": "full",
                        "p.limit": "1",
                        "p.nodedepth": "2",
                    }
                    u_results = cms.query(q_params)
                    if int(u_results.get("total")) > 0:
                        user = u_results.get("hits")[0]
                        full_name = user.get("rep:fullname")
                        group_name = grp.get("rep:principalName")
                        if not full_name:
                            full_name = user.get("rep:principalName")

                        try:
                            email = user.get("profile").get("email", "")
                        except AttributeError:
                            email = ""

                        # check if the user has a net id
                        match = re.search(
                            r"uid=(?P<netid>\w*)", user.get("rep:principalName")
                        )
                        netid = ""
                        if match:
                            netid = match.group("netid")

                        data.append([group_name, netid, full_name, email])
        except Exception as ex:
            # 'NoneType' object is not iterable - group with no members
            print(ex)

    # output
    if file_type.lower() == "xls":
        stdout = click.get_binary_stream("stdout")
        stdout.write(data.xls)
    else:
        stdout = click.get_text_stream("stdout")
        stdout.write(data.csv)
    stdout.close()


if __name__ == "__main__":
    main()

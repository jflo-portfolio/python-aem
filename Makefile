.DEFAULT_GOAL := help

pylint:
	pylint --rcfile=.pylintrc aem -f parseable -r n

pycodestyle:
	pycodestyle aem --max-line-length=120

pydocstyle:
	pydocstyle aem

lint: pylint pycodestyle pydocstyle

help:
	@(echo 'help')
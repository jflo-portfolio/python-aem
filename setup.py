from __future__ import print_function

from setuptools import find_packages, setup

setup(
    name="python-aem",
    version="0.0.2",
    packages=find_packages(exclude=("tests", "examples", "docs")),
    # license='',
    install_requires=["future", "requests"],
    tests_require=["pytest"],
    # description='',
    # long_description=open('README.txt').read(),
)

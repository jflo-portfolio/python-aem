"""AEM Module."""

from .api import Api as api
from .sling import SlingApi as sling
from .sling import SlingError

__version__ = "0.1.0"
VERSION = __version__
__all__ = ["api", "sling", "SlingError"]

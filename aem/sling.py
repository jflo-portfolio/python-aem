"""Module containing Sling Classes."""
import re
import urllib.parse
from builtins import str
from collections import OrderedDict

import requests
from future import standard_library
from requests.packages import urllib3

standard_library.install_aliases()

urllib3.disable_warnings()


class SlingError(Exception):
    """Base class for Sling errors."""

    def __init__(self, message, errors):
        """Create SlingError object."""
        super(SlingError, self).__init__(message)
        self.errors = errors
        self.args = (message, errors["status_code"])


class SlingApi:
    """Base class for the Sling API."""

    def __init__(self, sling_host, auth):
        """Create a Sling api object."""
        self.sling_host = sling_host
        self.session = requests.Session()
        req_adapter = requests.adapters.HTTPAdapter(max_retries=3)
        protocol = "{0}{1}".format(self.sling_host, "://")
        self.session.mount(protocol, req_adapter)
        self.auth = auth
        self.session.auth = auth
        self.request_url = None

    def get(self, path, params=None):
        """GET data.

        :param path: path
        :type path: str
        :param params: a dictionary of query parameters
        :type params: dict
        :returns: the content of the GET
        """
        get_url = urllib.parse.urljoin(str(self.sling_host), str(path))

        resp = self.session.get(get_url, params=params, verify=False)

        self.request_url = resp.url
        status_code = resp.status_code

        if urllib.parse.urlparse(get_url).path.endswith(".json"):
            content = resp.json(object_pairs_hook=OrderedDict)
            # content = resp.content
        else:
            content = resp.content
        try:
            resp.raise_for_status()
        except requests.exceptions.HTTPError:
            raise SlingError(
                "GET Error", {"status_code": status_code, "content": content}
            )
        return content

    def post(self, path, payload, files=None, charset="utf-8"):
        """POST data.

        :param path: path
        :type path: str
        :param payload: a dictionary of payload data
        :type payload: dict
        :param files: a dict with the key being the name of the form field and
        the value being either a string or a 2, 3 or 4-length tuple
        :type files: dict
        :param charset: a charset string
        :type charset: str
        :returns: a dictionary containing the status_code
        :rtype: dict
        """
        post_url = urllib.parse.urljoin(str(self.sling_host), str(path))
        payload["_charset_"] = charset

        resp = self.session.post(post_url, data=payload, files=files)
        status_code = resp.status_code
        content = resp.content

        try:
            resp.raise_for_status()
        except requests.exceptions.HTTPError:
            raise SlingError(
                "POST Error", {"status_code": status_code, "content": content}
            )

        return {"status_code": status_code}

    def copy(self, path, dest):
        """Copy a node.

        :param path: path of node to copy
        :type path: str
        :param payload: destination to copy the node
        :type payload: str
        :returns: a dictionary containing the status_code
        """
        payload = {":operation": "copy", ":dest": dest}

        try:
            return self.post(path, payload=payload)
        except SlingError as ex:
            error_message = "Copy Error"
            if ex.errors["status_code"] == 412:
                msg = "An item already exists at the destination."
                error_message = "{0}: {1}".format(error_message, msg)
            ex.message = error_message
            ex.args = (error_message, ex.errors["status_code"])
            raise

    def move(self, path, dest):
        """Move a node.

        :param path: path of node to move
        :type path: str
        :param dest: destination to move the node
        :type dest: str
        :returns: a dictionary containing the status_code
        """
        payload = {":operation": "move", ":dest": dest}

        try:
            return self.post(path, payload=payload)
        except SlingError as ex:
            error_message = "Move Error"
            if ex.errors["status_code"] == 412:
                msg = "An item already exists at the destination."
                error_message = "{0}: {1}".format(error_message, msg)
            ex.message = error_message
            ex.args = (error_message, ex.errors["status_code"])
            raise

    def delete(self, path):
        """Delete a node.

        :param path: path of node to delete
        :type path: str
        :returns: a dictionary containing the status_code
        """
        payload = {":operation": "delete"}

        try:
            return self.post(path, payload=payload)
        except SlingError as ex:
            error_message = "Delete Error"
            ex.message = error_message
            ex.args = (error_message, ex.errors["status_code"])
            raise

    def safe_move(self, path, dest):
        """'safe move' a node.

        This method will first copy the node first, then it will delete the
        original.

        :param path: path of node to move
        :type path: str
        :param dest: destination to move the node
        :type dest: str
        """
        try:
            self.copy(path, dest)
            self.delete(path)
        except SlingError as ex:
            error_message = "Safe Move Error"
            ex.message = error_message
            ex.args = (error_message, ex.errors["status_code"])
            raise

    def order(self, path, placement, sibling=None):
        """Order a node.

        :param path: path of node to move
        :type path: str
        :param placement: placement, either: 'first', 'last', 'before', 'after'
        or a number
        :type placement: str
        :returns: a dictionary containing the status_code
        """
        payload = dict()

        match = re.match(r"\d|first|last|before|after", placement)
        if match:
            if placement in ["before", "after"]:
                if sibling is not None:
                    payload[":order"] = "{0} {1}".format(placement, sibling)
                else:
                    raise ValueError(
                        "sibling node is required when placement\
                                      is either 'before' or 'after'"
                    )
            else:
                # for first, last or number
                if sibling is None:
                    payload[":order"] = "{0}".format(placement)

        else:
            raise ValueError(
                "placement must be:\
                             'first', 'last', 'before', 'after' or a number"
            )

        try:
            return self.post(path, payload=payload)
        except SlingError as ex:
            error_message = "Order Error"
            if ex.errors["status_code"] == 412:
                msg = "An item already exists at the destination."
                error_message = "{0}: {1}".format(error_message, msg)
            ex.message = error_message
            ex.args = (error_message, ex.errors["status_code"])
            raise

    def import_content(
        self, path, content, name=None, replace=False, replace_properties=False
    ):
        """Import content.

        :param path: path to install content
        :type path: str
        :returns: a dictionary containing the status_code
        """
        payload = {":operation": "import", ":contentType": "json", ":content": content}

        if name:
            payload[":name"] = name

        if replace:
            payload[":replace"] = "true"

        if replace_properties:
            payload[":replaceProperties"] = "true"

        try:
            return self.post(path, payload=payload)
        except SlingError as ex:
            error_message = "Import Error"
            if ex.errors["status_code"] == 412:
                msg = "An item already exists at the destination."
                error_message = "{0}: {1}".format(error_message, msg)
            ex.message = error_message
            ex.args = (error_message, ex.errors["status_code"])
            raise

    def import_file(
        self, path, file, name=None, replace=False, replace_properties=False
    ):
        """
        Import content from a file.

        :param path: path to install content
        :type path: str

        :param file: path to a json file to import
        :type path: str
        :returns: a dictionary containing the status_code
        """
        with open(file, "r") as fh:
            content = fh.read()

        return self.import_content(path, content, name, replace, replace_properties)

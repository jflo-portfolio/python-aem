"""Module for AEM API."""
import urllib.parse

from future import standard_library

from aem.sling import SlingApi

standard_library.install_aliases()


class Api(SlingApi):
    """Class for the AEM API."""

    def __init__(self, *args, **kwargs):
        """Create an AEM api object."""
        super(Api, self).__init__(*args, **kwargs)
        self.querybuilder_path = "/bin/querybuilder.json"

    def query_url(self, params):
        """Print out a querybuilder url.

        This is useful for when you want to share a query with others.

        :param params: a dictionary of query parameters
        :type params: dict
        :returns: an url string
        :rtype: str
        """
        param_string = urllib.parse.urlencode(params)
        return "{0}{1}?{2}".format(
            self.sling_host, self.querybuilder_path, param_string
        )

    def query(self, params):
        """Perform a query with the querybuilder.

        :param params: a dictionary of query parameters
        :type params: dict
        :returns: an OrderedDict of results
        :rtype: `collections.OrderedDict`
        """
        result = self.get(self.querybuilder_path, params=params)
        return result

    def activate(self, path):
        """Activate a node.

        :param path: the path of the node to activate
        :type path: str
        :returns: a dictionary containing the status_code
        :rtype: dict
        """
        payload = {"cmd": "activate", "path": path}
        replicate_url = "{0}{1}".format(self.sling_host, "/bin/replicate.json")
        return self.post(replicate_url, payload=payload)

    def deactivate(self, path):
        """Deactivate a node.

        :param path: the path of the node to deactivate
        :type path: str
        :returns: a dictionary containing the status_code
        :rtype: dict
        """
        payload = {"cmd": "deactivate", "path": path}
        replicate_url = "{0}{1}".format(self.sling_host, "/bin/replicate.json")
        return self.post(replicate_url, payload=payload)

    def tree_activate(
        self,
        path,
        dryrun=True,
        ignoredeactivated=True,
        reactivate=False,
        onlymodified=True,
    ):
        """Tree activate a path.

        :param path: the path to tree activate
        :type path: str
        :param dryrun: the path to tree activate
        :type dryrun: bool
        :param ignoredeactivated: ignore deactivated nodes?
        :type ignoredeactivated: str
        :param reactivate: reactivate nodes?
        :type reactivate: bool
        :param onlymodified: activate only modifed nodes?
        :type onlymodified: bool
        :returns: a dictionary containing the status_code
        :rtype: dict
        """
        payload = {"path": path}

        if dryrun:
            payload["cmd"] = "dryrun"
        else:
            payload["cmd"] = "activate"

        if ignoredeactivated:
            payload["ignoredeactivated"] = "true"
        else:
            payload["ignoredeactivated"] = "false"

        if onlymodified:
            payload["onlymodified"] = "true"
        else:
            payload["onlymodified"] = "false"

        if reactivate:
            payload["reactivate"] = "true"
        else:
            payload["reactivate"] = "false"

        activate_url = "{0}{1}".format(
            self.sling_host, "/etc/replication/treeactivation.html"
        )
        return self.post(activate_url, payload=payload)

    def upload(self, package, name="", install=False, force=True):
        """Upload a package.

        :param package: the file name of a package on the filesystem
        :type package: str
        :param name: name of the package
        :type name: str
        :param install: install package?
        :type install: bool
        :param force: force the install?
        :type force: bool
        :returns: a dictionary containing the status_code
        :rtype: dict
        """
        files = {"file": (name, open(package, "rb"), "application/octet-stream")}

        payload = {"cmd": "upload", "name": name}

        if install:
            payload["install"] = "true"
        else:
            payload["install"] = "false"

        if force:
            payload["force"] = "true"
        else:
            payload["force"] = "false"

        upload_url = "{0}{1}".format(self.sling_host, "/crx/packmgr/service.jsp")
        return self.post(upload_url, files=files, payload=payload)

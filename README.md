# python-aem

*No Longer Being Maintained*

python-aem is a Python library to be used with [Adobe Experience Manager].
This library allows you to manipulate content, install packages and query the datastore.

[Adobe Experience Manager]: http://www.adobe.com/marketing-cloud/experience-manager.html

## Installation

### Install Python and PIP

### Python version

Python (>= 3.6)

### On a Mac
Install Homebrew: http://brew.sh/

    brew install python

This should install both python and pip.

### On Linux
Using yum:

    yum install python
    yum install python-pip

Using apt-get:

    apt-get install python
    apt-get install python-pip

### Install python-aem

Install python-aem

    pip install git+https://bitbucket.org/jflo-portfolio/python-aem.git

## Quick Example

Import python-aem module

    >>> import aem

Create an aem api instance

    >>> cms = aem.api('http://localhost:4502', ('username', 'password'))

Query parameters as a python dictionary

    >>> params = {'type': 'cq:Page', 'orderby': '@jcr:content/cq:lastModified', 'path': '/content'}

Use the query method to run the AEM Querybuilder

    >>> results = cms.query(params)
    >>> for h in results.get('hits'):
            print(h.get('path', ''))

### API Methods
get - gets data from a node
cms.get(path)

post - posts data to a node
cms.post(path, payload=post_data)

copy - copies node from one place to another
cms.copy(src, dest)

move - moves node from one place to another
cms.move(src, dest)

delete - deletes a node
cms.delete(path)

safe_move - first moves src to dest, then deletes src
cms.save_move(src, dest)

query - uses the query builder to search the repository
cms.query(query_params)

order - orders a node
first - makes path the first of its siblings
cms.order(path, 'first')

last - makes path the last of its siblings
cms.order(path, 'last')

before - orders path before a specific node
cms.order(path, 'before', 'node1')

after - orders path after a specific node
cms.order(path, 'after', 'node1')

order number - orders path by number
cms.order(path, '3')

upload - uploads a package
cms.upload(package_file, install=False)

## More Examples
You can look in examples directory to see more.
